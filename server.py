# standard library
import os

# third-party
from flask import Flask
from flask_mongoengine import MongoEngine
from flask_restplus import Api
from werkzeug.contrib.fixers import ProxyFix

# local test_task
from test_task.api.v1.car import api as car_api

app = Flask(__name__)
app.config['MONGODB_SETTINGS'] = {
    'host': os.environ.get('MONGODB_URI', 'mongodb://127.0.0.1:27017/cars_db')
}
app.wsgi_app = ProxyFix(app.wsgi_app)
db = MongoEngine()
db.init_app(app)

api = Api(app, version='1.0', title='Cars API',
          description='A simple Cars API')
api.namespaces = []
api.add_namespace(car_api)

if __name__ == '__main__':
    app.run(debug=True)
