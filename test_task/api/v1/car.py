# third-party
from flask_restplus import Namespace, Resource, fields

# local test_task
from test_task.models.car import Car

api = Namespace('cars', description='Cars operations')

CarSerializer = api.model('Car', {
    'id': fields.String(
        readonly=True, description='The car unique identifier'),
    'type': fields.String(
        required=True, description='The car type'),
    'color': fields.String(
        required=False, description='The car color'),
    'created_at': fields.DateTime(
        readonly=True, required=False, description='The car created_at'),
    'updated_at': fields.DateTime(
        readonly=True, required=False,description='The car updated_at')
})


@api.route('/')
class CarList(Resource):
    model = Car

    """Shows a list of all cars, and lets you POST to add new car"""
    @api.doc('list_cars')
    @api.marshal_with(CarSerializer, envelope='resource')
    def get(self):
        """
        List of cars
        """

        return list(self.model.objects[:10])

    @api.doc('create_car')
    @api.expect(CarSerializer)
    @api.marshal_with(CarSerializer, code=201)
    def post(self):
        """Create a new car"""
        print(api.payload)
        if 'id' in api.payload:
            del api.payload['id']
        _car = Car(**api.payload)
        _car.save()
        return _car, 201


@api.route('/<string:id>')
@api.response(404, 'Car not found')
@api.param('id', 'The car identifier')
class CarResource(Resource):
    model = Car

    def get_object(self, id, **kwargs):
        obj = self.model.objects(id=id).first()
        if not obj:
            api.abort(404, "{} {} doesn't exist".format(
                self.model.__name__, id))
        return obj

    """Show a single car item and lets you delete them"""
    @api.doc('get_car')
    @api.marshal_with(CarSerializer)
    def get(self, id, **kwargs):
        """Fetch a given resource"""
        return self.get_object(id, **kwargs)

    @api.doc('delete_car')
    @api.response(204, 'Car deleted')
    def delete(self, id, **kwargs):
        """Delete a car given its identifier"""
        self.get_object(id, **kwargs).delete()
        return '', 204

    @api.expect(CarSerializer)
    @api.marshal_with(CarSerializer)
    def put(self, id, **kwargs):
        """Update a car given its identifier"""
        if 'id' in api.payload:
            del api.payload['id']
        obj = self.get_object(id, **kwargs)
        for key in api.payload.keys():
            obj.__setattr__(key, api.payload[key])
        obj.save()
        return obj
