# standard library
import datetime

# third-party
from mongoengine import DateTimeField, Document, StringField


class BaseDocument(Document):

    created_at = DateTimeField(default=datetime.datetime.utcnow)
    updated_at = DateTimeField(default=datetime.datetime.utcnow)

    meta = {
        'allow_inheritance': True,
        'abstract': True
    }


class Car(BaseDocument):
    type = StringField(required=True, max_length=200)
    color = StringField(max_length=200)

    def save(self, *args, **kwargs):
        self.updated_at = datetime.datetime.utcnow()

        return super(Car, self).save(*args, **kwargs)
